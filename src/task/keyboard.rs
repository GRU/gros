use conquer_once::spin::OnceCell;
use crossbeam_queue::ArrayQueue;
use crate::println;
use crate::{serial_print, serial_println};
use alloc::string::String;
use crate::alloc::string::ToString;
use crate::software::command_func;

use pc_keyboard::KeyCode;

static SCANCODE_QUEUE: OnceCell<ArrayQueue<u8>> = OnceCell::uninit();

pub struct ScancodeStream {
    _private: (),
}

impl ScancodeStream {
    pub fn new() -> Self {
        SCANCODE_QUEUE.try_init_once(|| ArrayQueue::new(100))
            .expect("ScancodeStream::new should only be called once");
        ScancodeStream { _private: () }
    }
}

use core::{pin::Pin, task::{Poll, Context}};
use futures_util::stream::Stream;

impl Stream for ScancodeStream {
    type Item = u8;

    fn poll_next(self: Pin<&mut Self>, cx: &mut Context) -> Poll<Option<u8>> {
        let queue = SCANCODE_QUEUE
            .try_get()
            .expect("scancode queue not initialized");

        // fast path
        if let Ok(scancode) = queue.pop() {
            return Poll::Ready(Some(scancode));
        }

        WAKER.register(&cx.waker());
        match queue.pop() {
            Ok(scancode) => {
                WAKER.take();
                Poll::Ready(Some(scancode))
            }
            Err(crossbeam_queue::PopError) => Poll::Pending,
        }
    }
}

use futures_util::task::AtomicWaker;

static WAKER: AtomicWaker = AtomicWaker::new();

pub(crate) fn add_scancode(scancode: u8) {
    if let Ok(queue) = SCANCODE_QUEUE.try_get() {
        if let Err(_) = queue.push(scancode) {
            println!("WARNING: scancode queue full; dropping keyboard input");
        } else {
            WAKER.wake(); // new
        }
    } else {
        println!("WARNING: scancode queue uninitialized");
    }
}

use futures_util::stream::StreamExt;
use pc_keyboard::{layouts, DecodedKey, HandleControl, Keyboard, ScancodeSet1};
use crate::print;

pub async fn process_keyboard_input() {
    let mut scancodes = ScancodeStream::new();
    let mut keyboard = Keyboard::new(layouts::Us104Key, ScancodeSet1,
        HandleControl::Ignore);
    let mut command = String::new();
    while let Some(scancode) = scancodes.next().await {
    	  if let Ok(Some(key_event)) = keyboard.add_byte(scancode) {
	     if let Some(key) = keyboard.process_keyevent(key_event) {
	     match key {   
                DecodedKey::Unicode(character) => (
					       match scancode {	  
					       28 => { // 28 is keycode for Enter key
					       	  println!();
						  command_func(command);
						  command = "".to_string();
						  print!("> ");
						}
						14 => { // 14 is keycode for Backspace key
						  command.pop();
						}
						scancode => {
						  print!("{}", character);
						  command.push(character);
						}
					}
		),
                DecodedKey::RawKey(key) => serial_print!("{:?}", key),
	     };
	     };
	 }
	}
}