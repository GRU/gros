use alloc::string::String;
use alloc::vec::Vec;
use crate::println;

pub fn calc(args: Vec<&str>) {
    if args.len() < 3 || args.len() > 3 {
       println!("Not enought or too much arguments");
       return;
    }
    let arg1: i32 = args[0].parse().unwrap();
    let arg2: i32 = args[2].parse().unwrap();
    let operation = args[1];
    if arg2 == 0 && operation == "/" {
       println!("Divide by zero");
       return;
    } else if arg2 == 0 && operation == "%" {
      println!("Modulo by zero");
      return;
    }
    match args[1] {
    	  "+" => {println!("{}", arg1+arg2)},
    	  "-" => {println!("{}", arg1-arg2)},
    	  "*" => {println!("{}", arg1*arg2)},
    	  "/" => {println!("{}", arg1/arg2)},
	  "%" => {println!("{}", arg1%arg2)},
	  arg => {println!("Someting went wrong")},
    }	  
}